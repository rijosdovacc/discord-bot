import random
from datetime import time, datetime, UTC
from typing import Optional

from discord import Intents, Embed, Role, Message, HTTPException
from discord.ext import commands
from discord.ext.commands import CommandError
from pydantic import BaseModel, Field

from settings import settings

intents = Intents.default()
intents.message_content = True
intents.reactions = True
intents.members = True
bot_client = commands.Bot(command_prefix=commands.when_mentioned_or('!'), intents=intents)


def elo_calculation(member_server_roles: list[Role]) -> int:
    def safe_int(value: str) -> int:
        try:
            return int(value)
        except ValueError:
            return 0

    return sum([safe_int(role.name) for role in member_server_roles])


class Player(BaseModel):
    guild_id: int
    client_id: int
    global_name: str
    server_name: str
    nick_name: Optional[str] = None
    joined_at: datetime

    def __eq__(self, other):
        return self.client_id == other.client_id

    @property
    def name(self):
        return self.nick_name or self.server_name or self.global_name

    def mention(self) -> str:
        return f'<@{self.client_id}>'

    def __repr__(self):
        return f"{self.name} ({self.elo})"

    def __str__(self):
        return f"{self.mention()} ({self.elo})"

    @property
    def elo(self):
        guild = bot_client.get_guild(self.guild_id)
        member = guild.get_member(self.client_id)
        return elo_calculation(member.roles)

    class Config:
        arbitrary_types_allowed = True


class GamePool(BaseModel):
    players: list[Optional[Player]] = []
    guild_id: int = Field(frozen=True)
    channel_id: int = Field(frozen=True)
    open_message: Message = Field(frozen=True)
    target_time: time
    announcement_message: Optional[Message] = None
    teams: Optional[tuple[list, list]] = None

    @property
    def identifier(self):
        return hash((self.guild_id, self.channel_id))

    class Config:
        arbitrary_types_allowed = True


class PoolAlreadyExists(Exception):
    def __init__(self, existing_pool: GamePool):
        self.existing_pool = existing_pool
        super().__init__(
            f"There is already a pool active ({existing_pool.target_time.strftime("%H:%MLCL")}) in this channel. "
            f"Look in https://discord.com/channels/{existing_pool.guild_id}/"
            f"{existing_pool.channel_id}/{existing_pool.announcement_message.id}")


class NoActivePool(Exception):
    pass


class PlayerNotInPool(Exception):
    pass


class PlayerAlreadyInPool(Exception):
    pass


class NotEnoughPlayers(Exception):
    pass


POOLS: dict[int, GamePool] = {}


@bot_client.group(name="pool", invoke_without_command=True)
async def dc_pool_group(ctx):
    if ctx.invoked_subcommand is None:
        await ctx.send(
            'Available commands:'
            '\n - !pool open now'
            '\n - !pool open hhmm or hh:mm'
            '\n - !pool teams'
            '\n - !pool split'
            '\n - !pool close', delete_after=5)


@dc_pool_group.group(name="open", invoke_without_command=True)
async def dc_open_pool(ctx, *args):
    if ctx.invoked_subcommand is None and args:
        argument = "".join(args)
        try:
            parsed_time = time.fromisoformat(argument)
        except ValueError:
            return await ctx.message.reply("Invalid time. Make sure it's iso format. Examples: 2130 | 21:30 | 21 30",
                                           delete_after=5)
        await create_pool_and_add_player(ctx, parsed_time)
    else:
        await ctx.message.reply('Available commands:'
                                '\n - !pool open now'
                                '\n - !pool open hhmm or hh:mm', delete_after=5)


@dc_open_pool.command(name="now")
async def dc_open_pool_now(ctx):
    await create_pool_and_add_player(ctx)


@dc_pool_group.group(name="close")
async def dc_close_pool(ctx):
    try:
        closed_pool = close_pool(guild_id=ctx.guild.id, channel_id=ctx.channel.id)
        await ctx.message.reply(
            f"Closed pool https://discord.com/channels/{closed_pool.guild_id}/"
            f"{closed_pool.channel_id}/{closed_pool.open_message.id}")
    except NoActivePool as error:
        await ctx.message.reply(error, delete_after=5)


@bot_client.listen("on_raw_reaction_add")
async def dc_on_reaction_add(raw_reaction_action_event):
    if raw_reaction_action_event.emoji.name != "👍":
        return
    pool = get_pool(announcement_message_id=raw_reaction_action_event.message_id,
                    guild_id=raw_reaction_action_event.guild_id, channel_id=raw_reaction_action_event.channel_id)
    if not pool:
        return
    this_player = Player(guild_id=pool.guild_id,
                         client_id=raw_reaction_action_event.member.id,
                         global_name=raw_reaction_action_event.member.global_name,
                         server_name=raw_reaction_action_event.member.name,
                         nick_name=raw_reaction_action_event.member.nick,
                         joined_at=datetime.now(UTC))
    try:
        add_player_to_pool(identifier=pool.identifier, player=this_player)
    except PlayerAlreadyInPool:
        return
    await update_announcement(pool=pool)


@bot_client.listen("on_raw_reaction_remove")
async def dc_on_reaction_remove(raw_reaction_action_event):
    if raw_reaction_action_event.emoji.name != "👍":
        return
    pool = get_pool(announcement_message_id=raw_reaction_action_event.message_id,
                    guild_id=raw_reaction_action_event.guild_id, channel_id=raw_reaction_action_event.channel_id)
    if not pool:
        return
    remove_player_from_pool(identifier=pool.identifier, player_client_id=raw_reaction_action_event.user_id)
    await update_announcement(pool=pool)


@bot_client.listen("on_raw_reaction_clear")
async def dc_on_reaction_clear(raw_reaction_clear_event):
    pool = get_pool(announcement_message_id=raw_reaction_clear_event.message_id,
                    guild_id=raw_reaction_clear_event.guild_id, channel_id=raw_reaction_clear_event.channel_id)
    if not pool:
        return
    clear_players_from_pool(identifier=pool.identifier)
    await update_announcement(pool=pool)


@bot_client.listen("on_member_update")
async def dc_on_member_update(before, after):
    mapping = {identifier: any(filter(lambda player: player.client_id == after.id, body.players)) for
               identifier, body in POOLS.items()}
    for identifier, result in mapping.items():
        if not result:
            continue
        pool = get_pool(pool_identifier=identifier)
        await update_announcement(pool=pool)


MAPS = ["Dust 2", "Mirage", "Inferno", "Nuke", "Overpass", "Vertigo", "Ancient", "Train", "Office", "Anubis"]


@dc_pool_group.command(name="teams")
async def dc_pool_teams(ctx):
    pool = get_pool(guild_id=ctx.guild.id, channel_id=ctx.channel.id)
    if not pool:
        return await ctx.message.reply("Is there an active pool?", delete_after=5)
    try:
        team1, team2 = make_teams(players=pool.players)
        pool_add_teams(pool.identifier, (team1, team2))
    except NotEnoughPlayers as error:
        return await ctx.message.reply(error)
    await ctx.message.delete()
    embed = Embed(timestamp=datetime.now(UTC))
    embed.set_footer(text='Generated')
    embed.add_field(name='Team 1 ELO', value=sum([p.elo for p in team1]))
    embed.add_field(name='Team 2 ELO', value=sum([p.elo for p in team2]))

    embed.add_field(name='Team 1 - Join as CT', value="\n".join([repr(p) for p in team1]), inline=False)
    embed.add_field(name='Team 2 - Join as T', value="\n".join([repr(p) for p in team2]), inline=False)
    
    embed.add_field(name='Map suggestion', value=random.choice(MAPS))

    return await pool.announcement_message.reply(embed=embed)


@dc_pool_group.command(name='split')
async def split_teams_to_channels(ctx):
    pool = get_pool(guild_id=ctx.guild.id, channel_id=ctx.channel.id)
    if not pool:
        return await ctx.message.reply("Is there an active pool?", delete_after=5)
    if not pool.teams:
        return await ctx.message.reply("Have teams been created yet?", delete_after=5)
    try:
        channels = {voice_channel.name.upper(): voice_channel for voice_channel in ctx.guild.voice_channels if
                    voice_channel.name.upper() in ["TEAM 1", "TEAM 2"]}
        assert len(channels) == 2
    except AssertionError:
        raise CommandError('Make sure two voice channels exist named `Team 1` and `Team 2`')
    team1, team2 = pool.teams
    for player in team1:
        try:
            await ctx.guild.get_member(player.client_id).move_to(channels.get("TEAM 1"))
        except HTTPException:
            continue
    for player in team2:
        try:
            await ctx.guild.get_member(player.client_id).move_to(channels.get("TEAM 2"))
        except HTTPException:
            continue
    await ctx.message.reply('Players have been moved!')


@bot_client.command(name='ping', hidden=True)
async def dc_ping(ctx):
    """memes"""
    await ctx.message.delete()
    await ctx.send('** _Sim?_ **:face_with_monocle:', delete_after=5)


@bot_client.command(name='open', hidden=True)
async def dc_deprecated_open(ctx):
    await ctx.message.delete()
    await ctx.send('This command is deprecated. Use `!pool open` instead', delete_after=5)


@bot_client.command(name='teams', hidden=True)
async def dc_deprecated_teams(ctx):
    await ctx.message.delete()
    await ctx.send('This command is deprecated. Use `!pool teams` instead', delete_after=5)


@bot_client.command(name='split', hidden=True)
async def dc_deprecated_split(ctx):
    await ctx.message.delete()
    await ctx.send('This command is deprecated. Use `!pool split` instead', delete_after=5)


def get_role(guild_roles: list):
    try:
        return next(filter(lambda x: settings.annoucement_role_name in x.name.upper(), guild_roles))
    except StopIteration:
        return None


def create_pool(guild_id: int, channel_id: int, open_message: Message, target_time: time):
    this_pool = GamePool(guild_id=guild_id, channel_id=channel_id, open_message=open_message,
                         target_time=target_time)
    existing_pool = list(filter(lambda pool: pool.identifier == this_pool.identifier, POOLS.values()))
    if existing_pool:
        raise PoolAlreadyExists(existing_pool=existing_pool[-1])
    POOLS[this_pool.identifier] = this_pool
    return this_pool


def close_pool(guild_id: int, channel_id: int):
    this_pool_identifier = hash((guild_id, channel_id))
    existing_pool = list(filter(lambda pool: pool.identifier == hash((guild_id, channel_id)), POOLS.values()))
    if not existing_pool:
        raise NoActivePool("There is currently no active pool on this channel...")
    POOLS.pop(this_pool_identifier)
    return existing_pool[-1]


def add_player_to_pool(identifier: int, player: Player) -> list[Optional[Player]]:
    this_pool = POOLS.get(identifier)
    if not this_pool:
        raise NoActivePool("There is no pool with that identifier?!")
    if player in this_pool.players:
        raise PlayerAlreadyInPool("Player already in this pool...")
    this_pool.players.append(player)
    return this_pool.players


def remove_player_from_pool(identifier: int, player_client_id: int) -> list[Optional[Player]]:
    this_pool = POOLS.get(identifier)
    if not this_pool:
        raise NoActivePool("There is no pool with that identifier?!")
    this_player = list(filter(lambda player: player.client_id == player_client_id, this_pool.players))
    if not this_player:
        raise PlayerNotInPool("What?")
    this_pool.players.remove(this_player[-1])
    return this_pool.players


def clear_players_from_pool(identifier: int) -> list[Optional[Player]]:
    this_pool = POOLS.get(identifier)
    if not this_pool:
        raise NoActivePool("There is no pool with that identifier?!")
    this_pool.players.clear()
    return this_pool.players


def add_announcement_info(identifier: int, message: Message):
    this_pool = POOLS.get(identifier)
    if not this_pool:
        raise NoActivePool("There is no pool with that identifier?!")
    this_pool.announcement_message = message
    return this_pool.announcement_message.id


def pool_add_teams(identifier: int, teams: tuple[list, list]):
    this_pool = POOLS.get(identifier)
    if not this_pool:
        raise NoActivePool("There is no pool with that identifier?!")
    this_pool.teams = teams
    return this_pool.teams


async def create_pool_and_add_player(ctx, target_time: Optional[time] = None):
    try:
        new_pool = create_pool(guild_id=ctx.guild.id, channel_id=ctx.channel.id, open_message=ctx.message,
                               target_time=target_time or ctx.message.created_at.time())
    except PoolAlreadyExists as active_pool:
        return await ctx.message.reply(active_pool)
    this_player = Player(guild_id=ctx.guild.id, client_id=ctx.message.author.id,
                         global_name=ctx.message.author.global_name,
                         server_name=ctx.message.author.name,
                         nick_name=ctx.message.author.nick,
                         joined_at=datetime.now(UTC))
    players = add_player_to_pool(identifier=new_pool.identifier, player=this_player)
    embed = Embed(title=f"Start @ {new_pool.target_time.strftime("%H:%M")} LCL", timestamp=datetime.now(UTC))
    embed.set_footer(text='Last edit')
    embed.add_field(name=f"Players in pool `{len(players)}`", value="\n".join([str(player) for player in players]))
    embed.add_field(name="Total ELO", value=sum([player.elo for player in players]))
    response = await ctx.message.reply(
        content=f"<@&{get_role(ctx.message.guild.roles).id}> Pool Open | Like this message to join!", embed=embed)
    add_announcement_info(identifier=new_pool.identifier, message=response)


async def update_announcement(pool: GamePool):
    embed = Embed(title=f"Start @ {pool.target_time.strftime("%H:%M")} LCL", timestamp=datetime.now(UTC))
    embed.set_footer(text='Last edit')
    embed.add_field(name=f"Players in pool `{len(pool.players)}`",
                    value="\n".join([str(player) for player in pool.players]))
    embed.add_field(name="Total ELO", value=sum([player.elo for player in pool.players]))
    await pool.announcement_message.edit(embed=embed)


def make_teams(players: list[Player]) -> tuple[list, list]:
    # if len(players) <= 1:
    #     raise NotEnoughPlayers("At least two players are required to form teams.")

    sorted_players = sorted(players, key=lambda player: player.elo, reverse=True)

    team1, team2 = [], []
    team1_elo, team2_elo = 0, 0

    for player in sorted_players:
        if team1_elo <= team2_elo:
            team1.append(player)
            team1_elo += player.elo
        else:
            team2.append(player)
            team2_elo += player.elo

    for _ in range(10):
        for player1 in team1[:]:
            for player2 in team2[:]:
                new_team1_elo = team1_elo - player1.elo + player2.elo
                new_team2_elo = team2_elo - player2.elo + player1.elo
                if abs(new_team1_elo - new_team2_elo) < abs(team1_elo - team2_elo):
                    team1.remove(player1)
                    team2.remove(player2)
                    team1.append(player2)
                    team2.append(player1)
                    team1_elo, team2_elo = new_team1_elo, new_team2_elo

    return team1, team2


def get_pool(announcement_message_id: Optional[int] = None, guild_id: Optional[int] = None,
             channel_id: Optional[int] = None, pool_identifier: Optional[int] = None) -> None | GamePool:
    try:
        return next(filter(lambda pool: pool.identifier == pool_identifier or
                                        pool.announcement_message.id == announcement_message_id or (
                                            pool.guild_id == guild_id and pool.channel_id == channel_id),
                           POOLS.values()))
    except StopIteration:
        return None


bot_client.run(settings.discord_bot_token)
