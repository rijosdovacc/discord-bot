from dotenv import load_dotenv
from pydantic import Extra
from pydantic_settings import BaseSettings

load_dotenv()


class Settings(BaseSettings, extra=Extra.allow):
    debug: bool = True

    discord_bot_token: str

    annoucement_role_name: str = "CS:GO"

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
